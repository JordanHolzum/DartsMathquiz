﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;


namespace DartsApp.Models
{
    public class Round
    {
        private Random random = new Random();

        private int[] possibleFields = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25 };
        public int Throw1 { get; set; }
        public int Throw2 { get; set; }
        public int Throw3 { get; set; }
        public int Factor1 { get; set; } = 1;
        public int Factor2 { get; set; } = 1;
        public int Factor3 { get; set; } = 1;
        public string Factor1Display { get; set; }
        public string Factor2Display { get; set; }
        public string Factor3Display { get; set; }       
        public int ResultFinal { get; set; }
        public int Guess { get; set; }
        public int ValueNum { get; set; }
        public int ValueFactor { get; set; } = 1;
        public string DisplayFactor { get; set; }
        public bool DisabledTextArea { get; set; }

        
        public int NumberGeneration()
        {
            int position = random.Next(possibleFields.Length);
            ValueNum = possibleFields[position];
            return ValueNum;
        }
        public string DisplayFactorGeneration()
        {
            if (ValueFactor == 1)
            {
                DisplayFactor = "S";
            }
            else if (ValueFactor == 2)
            {
                DisplayFactor = "D";
            }
            else if (ValueFactor == 3)
            {
                DisplayFactor = "T";
            }
            return DisplayFactor;
        }
        public int FactorGeneration()
        {
            ValueFactor = random.Next(1, 4);
            return ValueFactor;
        }
        public void Throw()
        {            
                Throw1 = NumberGeneration();
                Factor1 = FactorGeneration();
                Factor1Display = DisplayFactorGeneration();
                Throw2 = NumberGeneration();
                Factor2 = FactorGeneration();
                Factor2Display = DisplayFactorGeneration();
                Throw3 = NumberGeneration();
                Factor3 = FactorGeneration();
                Factor3Display = DisplayFactorGeneration();
                ResultFinal = Throw1 * Factor1 + Throw2 * Factor2 + Throw3 * Factor3;                 
        }
        public bool CheckGuess()
        {
            return ResultFinal == Guess;
        }

    }


}

