﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DartsMathquiz.Models
{
    public class RoundCalc
    {
        // Liste und durschnitt /Würfe
        public int Throw1{ get; set; }
        public int Throw2 { get; set; }
        public int Throw3 { get; set; }

       
        public int Result { get; set; }
        public void AddThrows()
        {
            Result = Throw1 + Throw2 + Throw3;
        }


    }
}
