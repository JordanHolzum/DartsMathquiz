#pragma checksum "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cf5da0da76b2ffae23cd985bc3ab853c35ff602b"
// <auto-generated/>
#pragma warning disable 1591
namespace DartsMathquiz.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using DartsMathquiz;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\_Imports.razor"
using DartsMathquiz.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
using DartsApp.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
using System.ComponentModel;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/dartsmathquiz")]
    public partial class DartsQuiz : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "table");
            __builder.AddAttribute(1, "class", "table");
            __builder.AddMarkupContent(2, "<thead><tr><th>1. Wurf</th>\r\n            <th>2. Wurf</th>\r\n            <th>3. Wurf</th>\r\n            <th>Ergebnis</th></tr></thead>\r\n    ");
            __builder.OpenElement(3, "tbody");
#nullable restore
#line 14 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
         foreach (var round in Rounds)
        {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(4, "tr");
            __builder.OpenElement(5, "td");
            __builder.AddContent(6, 
#nullable restore
#line 17 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                     round.Factor1Display

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(7, " ");
            __builder.AddContent(8, 
#nullable restore
#line 17 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                           round.Throw1

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(9, "\r\n                ");
            __builder.OpenElement(10, "td");
            __builder.AddContent(11, 
#nullable restore
#line 18 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                     round.Factor2Display

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(12, " ");
            __builder.AddContent(13, 
#nullable restore
#line 18 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                           round.Throw2

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(14, "\r\n                ");
            __builder.OpenElement(15, "td");
            __builder.AddContent(16, 
#nullable restore
#line 19 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                     round.Factor3Display

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(17, " ");
            __builder.AddContent(18, 
#nullable restore
#line 19 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                           round.Throw3

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n                ");
            __builder.OpenElement(20, "td");
            __builder.OpenElement(21, "input");
            __builder.AddAttribute(22, "disabled", 
#nullable restore
#line 20 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                      round.DisabledTextArea

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(23, "type", "number");
            __builder.AddAttribute(24, "min", "0");
            __builder.AddAttribute(25, "max", "180");
            __builder.AddAttribute(26, "onkeyup", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.KeyboardEventArgs>(this, 
#nullable restore
#line 20 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                                                                              (e) => { if (e.Key == "Enter" ) { CheckResult(round);} }

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(27, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 20 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                                                     round.Guess

#line default
#line hidden
#nullable disable
            , culture: global::System.Globalization.CultureInfo.InvariantCulture));
            __builder.AddAttribute(28, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => round.Guess = __value, round.Guess, culture: global::System.Globalization.CultureInfo.InvariantCulture));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 22 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
        }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(29, "\r\n    ");
            __builder.OpenElement(30, "tbody");
            __builder.OpenElement(31, "tr");
            __builder.OpenElement(32, "td");
            __builder.AddAttribute(33, "style", "color:mediumorchid");
            __builder.AddMarkupContent(34, "Übersprungen: ");
            __builder.AddContent(35, 
#nullable restore
#line 26 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                          amountSkipped

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(36, " von 3");
            __builder.CloseElement();
            __builder.AddMarkupContent(37, "\r\n            ");
            __builder.OpenElement(38, "td");
            __builder.AddAttribute(39, "style", "color:red");
            __builder.AddContent(40, "Falsche Ergebnisse: ");
            __builder.AddContent(41, 
#nullable restore
#line 27 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                       wrongAnswers

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(42, "\r\n            ");
            __builder.OpenElement(43, "td");
            __builder.AddAttribute(44, "style", "color:green");
            __builder.AddContent(45, "Richtige Ergebnisse: ");
            __builder.AddContent(46, 
#nullable restore
#line 28 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                          correctAnwers

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
#nullable restore
#line 29 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
             if (isCorrect == false)
            {

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(47, "<td style=\"color:red\">Ergebnis nicht korrekt</td>");
#nullable restore
#line 32 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
            }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 37 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
 if (hasStarted==false) {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(48, "button");
            __builder.AddAttribute(49, "style", "text-align:center");
            __builder.AddAttribute(50, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 37 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                      StartGame

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(51, "Start");
            __builder.CloseElement();
#nullable restore
#line 37 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                                               }

#line default
#line hidden
#nullable disable
#nullable restore
#line 39 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
 if (hasStarted && skipNotPossible==false)
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(52, "button");
            __builder.AddAttribute(53, "style", " background-color:mediumorchid; color:white");
            __builder.AddAttribute(54, "disabled", 
#nullable restore
#line 40 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                        skipNotPossible

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(55, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 40 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                                                   Skip

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(56, "Überspringen");
            __builder.CloseElement();
#nullable restore
#line 40 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                                                                              }

#line default
#line hidden
#nullable disable
#nullable restore
#line 42 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
 if (skipNotPossible)
{

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(57, "<p style=\"color:mediumorchid\">Limit erreicht! Überspringen nicht mehr möglich!</p>");
#nullable restore
#line 43 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                                                   }

#line default
#line hidden
#nullable disable
#nullable restore
#line 45 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
 if (hasStarted)
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(58, "button");
            __builder.AddAttribute(59, "class", "btn-light");
            __builder.AddAttribute(60, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 46 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                     Restart

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(61, "Restart");
            __builder.CloseElement();
#nullable restore
#line 46 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
                                                              }

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 49 "C:\Users\JordanHolzum\source\repos\DartsMathquiz\DartsMathquiz\Pages\DartsQuiz.razor"
 
    List<Round> Rounds = new List<Round>();
    public bool hasStarted = false;
    public bool isCorrect = true;
    public bool skipNotPossible = false;
    public int amountSkipped;
    public int wrongAnswers;
    int correctAnwers;

    public void StartGame()
    {
        hasStarted = true;
        Round round = new Round();
        round.Throw();
        Rounds.Add(round);
    }

    private void Skip()
    {
        amountSkipped++;
        StartGame();
        if (amountSkipped == 3)
        {
            skipNotPossible = true;
        }
    }

    private void CheckResult(Round round)
    {
        if (round.CheckGuess())
        {
            isCorrect = true;
            round.DisabledTextArea = true;
            correctAnwers++;
            StartGame();
        }
        else
        {
            wrongAnswers++;
            isCorrect = false;
        }
    }

    public void Restart()
    {
        hasStarted = false;
        skipNotPossible = false;
        amountSkipped = 0;
        wrongAnswers = 0;
        correctAnwers = 0;
        Rounds.Clear();
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
